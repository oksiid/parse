import urllib
import  requests
from bs4 import BeautifulSoup
import os

def ppp():
    ex_silka = ""
    f = open('links.txt', 'r').read().split('\n')
    f_1 = open('phones.txt', 'r').read().split('\n')
    f_2 = open('except.txt', 'w')
    for links_arr in f:
        link = links_arr
        print(link)

        try:
            page = requests.get(link)
            soup = BeautifulSoup(page.text, 'html.parser')
            #Параметры анкеты
            name = soup.find(class_='title-block').h1.text
            name = str(name).split('Индивидуалка ')
            name = name[1].split(' из Москвы')
            name = name[0]
            phone_number = f_1[f.index(links_arr)]
            print(phone_number)
            active_anketa =True
            moderation =True
            photo_approved = soup.find(class_='btn-photo-resp')
            if photo_approved == None:
                photo_approved = False
            else:
                photo_approved = True

            #Получние местоположения
            x=0
            place = []
            for mesto in soup.find_all(class_='row'):
                if x>2:
                    break
                mesto = str(mesto.b).split('<b>')
                mesto = mesto[1].split('</b>')
                place.append(mesto[0])
                x += 1

            city = place[0]
            metro = place[1]
            area = place[2]

            # Получение параметров девушки
            s = soup.find(class_='params-block')
            s = s.find_all(class_='row')
            param_age = str(s[0]).split('</span><span><span>')
            param_age = param_age[1].split('</span>')
            girl_age = int(param_age[0])

            param_height = str(s[1]).split('</span><span><span>')
            param_height = param_height[1].split('</span>')
            girl_height = int(param_height[0])

            param_weight = str(s[3]).split('</span><span><span>')
            param_weight = param_weight[1].split('</span>')
            girl_weight = int(param_weight[0])

            param_chest = str(s[2]).split('</span><span><span>')
            param_chest = param_chest[1].split('</span>')
            girl_chest = int(param_chest[0])

            #Получение параметров выезда
            s = soup.find(class_='chk-box')
            exits = []
            for inp in s.find_all('input'):
                if 'checked' in str(inp):
                    exits.append('True')
                else:
                    exits.append('False')

            exit_apartment = exits[0]
            exit_hotel = exits[1]
            exit_sauna = exits[2]
            exit_office = exits[3]

            #Получение есть ли свои апартаменты
            s = soup.find(class_='btn-appatment-resp active')
            if s == None:
                her_apartment = 'False'
            else:
                her_apartment = 'True'

            # Получение доп параметров
            s = soup.find(class_='more-block')
            s = s.find_all(class_='row')

            param_hair_color = str(s[0]).split('</span><span>')
            param_hair_color = str(param_hair_color[1]).split('</span>')
            hair_color = param_hair_color[0]

            param_intimate_haircut = str(s[1]).split('</span><span>')
            param_intimate_haircut = str(param_intimate_haircut[1]).split('</span>')
            intimate_haircut = param_intimate_haircut[0]

            param_nationality = str(s[2]).split('</span><span>')
            param_nationality = str(param_nationality[1]).split('</span>')
            nationality = param_nationality[0]

            #Тарифы

            # Получение тарифов
            s = soup.find_all(class_='price')

            tariff_apartment_1 = s[0].text
            tariff_apartment_1 = tariff_apartment_1.replace('\n', '')
            tariff_apartment_1 = tariff_apartment_1.replace('\t', '')
            tariff_apartment_1 = tariff_apartment_1.replace(' ', '')
            try:
                tariff_apartment_1 = int(tariff_apartment_1)
            except:
                tariff_apartment_1 = 0

            tariff_apartment_2 = s[1].text
            tariff_apartment_2 = tariff_apartment_2.replace('\n', '')
            tariff_apartment_2 = tariff_apartment_2.replace('\t', '')
            tariff_apartment_2 = tariff_apartment_2.replace(' ', '')
            try:
                tariff_apartment_2 = int(tariff_apartment_2)
            except:
                tariff_apartment_2 = 0

            tariff_apartment_night = s[2].text
            tariff_apartment_night = tariff_apartment_night.replace('\n', '')
            tariff_apartment_night = tariff_apartment_night.replace('\t', '')
            tariff_apartment_night = tariff_apartment_night.replace(' ', '')
            try:
                tariff_apartment_night = int(tariff_apartment_night)
            except:
                tariff_apartment_night = 0

            tariff_exit_1 = s[4].text
            tariff_exit_1 = tariff_exit_1.replace('\n', '')
            tariff_exit_1 = tariff_exit_1.replace('\t', '')
            tariff_exit_1 = tariff_exit_1.replace(' ', '')
            try:
                tariff_exit_1 = int(tariff_exit_1)
            except:
                tariff_exit_1 = 0

            tariff_exit_2 = s[5].text
            tariff_exit_2 = tariff_exit_2.replace('\n', '')
            tariff_exit_2 = tariff_exit_2.replace('\t', '')
            tariff_exit_2 = tariff_exit_2.replace(' ', '')
            try:
                tariff_exit_2 = int(tariff_exit_2)
            except:
                tariff_exit_2 = 0

            tariff_exit_night = s[6].text
            tariff_exit_night = tariff_exit_night.replace('\n', '')
            tariff_exit_night = tariff_exit_night.replace('\t', '')
            tariff_exit_night = tariff_exit_night.replace(' ', '')
            try:
                tariff_exit_night = int(tariff_exit_night)
            except:
                tariff_exit_night = 0

            tariff_anal = s[3].text
            tariff_anal = tariff_anal.replace('\n', '')
            tariff_anal = tariff_anal.replace('\t', '')
            tariff_anal = tariff_anal.replace(' ', '')

            try:
                tariff_anal = tariff_anal.replace('+', '')
                tariff_anal = int(tariff_anal)
            except:
                tariff_anal = 0

            #Эссе
            try:
                s = soup.find('p', class_='detail-text-full')

                essay = s.text
                essay = essay.replace('скрыть', '')
                essay = essay.replace('\n', '')
                essay = essay.replace('\t', '')
                essay = essay.replace('\r', '')
            except:
                essay = 'Я жду твоего звонка!'

            #Услуги
        ####################################
            s = soup.find(id='paramU_SX_C')
            if 'checked' in str(s):
                usluga_type_1 = 'True'
            else:
                usluga_type_1 = 'False'

            s = soup.find(id='paramU_SX_A')
            if 'checked' in str(s):
                usluga_type_2 = 'True'
            else:
                usluga_type_2 = 'False'

            s = soup.find(id='paramU_SX_G')
            if 'checked' in str(s):
                usluga_type_3 = 'True'
            else:
                usluga_type_3 = 'False'

            s = soup.find(id='paramU_SX_L')
            if 'checked' in str(s):
                usluga_type_4 = 'True'
            else:
                usluga_type_4 = 'False'

        #############################################

            s = soup.find(id='paramU_MN_P')
            if 'checked' in str(s):
                usluga_laska_1 = 'True'
            else:
                usluga_laska_1 = 'False'

            s = soup.find(id='paramU_MN_BP')
            if 'checked' in str(s):
                usluga_laska_2 = 'True'
            else:
                usluga_laska_2 = 'False'

            s = soup.find(id='paramU_MN_G')
            if 'checked' in str(s):
                usluga_laska_3 = 'True'
            else:
                usluga_laska_3 = 'False'

            s = soup.find(id='paramU_MN_VM')
            if 'checked' in str(s):
                usluga_laska_4 = 'True'
            else:
                usluga_laska_4 = 'False'

            s = soup.find(id='paramU_MN_K')
            if 'checked' in str(s):
                usluga_laska_5 = 'True'
            else:
                usluga_laska_5 = 'False'

            s = soup.find(id='paramU_MN_A')
            if 'checked' in str(s):
                usluga_laska_6 = 'True'
            else:
                usluga_laska_6 = 'False'

        #############################################

            s = soup.find(id='paramU_EX_B')
            if 'checked' in str(s):
                usluga_sado_mazo_1 = 'True'
            else:
                usluga_sado_mazo_1 = 'False'

            s = soup.find(id='paramU_EX_G')
            if 'checked' in str(s):
                usluga_sado_mazo_2 = 'True'
            else:
                usluga_sado_mazo_2 = 'False'

            s = soup.find(id='paramU_EX_RG')
            if 'checked' in str(s):
                usluga_sado_mazo_3 = 'True'
            else:
                usluga_sado_mazo_3 = 'False'

            s = soup.find(id='paramU_EX_LD')
            if 'checked' in str(s):
                usluga_sado_mazo_4 = 'True'
            else:
                usluga_sado_mazo_4 = 'False'

            s = soup.find(id='paramU_EX_P')
            if 'checked' in str(s):
                usluga_sado_mazo_5 = 'True'
            else:
                usluga_sado_mazo_5 = 'False'

            s = soup.find(id='paramU_EX_R')
            if 'checked' in str(s):
                usluga_sado_mazo_6 = 'True'
            else:
                usluga_sado_mazo_6 = 'False'

            s = soup.find(id='paramU_EX_F')
            if 'checked' in str(s):
                usluga_sado_mazo_7 = 'True'
            else:
                usluga_sado_mazo_7 = 'False'

            s = soup.find(id='paramU_EX_TR')
            if 'checked' in str(s):
                usluga_sado_mazo_8 = 'True'
            else:
                usluga_sado_mazo_8 = 'False'

        ############################################

            s = soup.find(id='paramU_FT_A')
            if 'checked' in str(s):
                usluga_fisting_1 = 'True'
            else:
                usluga_fisting_1 = 'False'

            s = soup.find(id='paramU_FT_C')
            if 'checked' in str(s):
                usluga_fisting_2 = 'True'
            else:
                usluga_fisting_2 = 'False'

        ############################################

            s = soup.find(id='paramU_ZD_V')
            if 'checked' in str(s):
                usluga_zolotoy_1 = 'True'
            else:
                usluga_zolotoy_1 = 'False'

            s = soup.find(id='paramU_ZD_P')
            if 'checked' in str(s):
                usluga_zolotoy_2 = 'True'
            else:
                usluga_zolotoy_2 = 'False'

        ############################################

            s = soup.find(id='paramU_AD_EX')
            if 'checked' in str(s):
                usluga_dopoln_1 = 'True'
            else:
                usluga_dopoln_1 = 'False'

            s = soup.find(id='paramU_AD_PV')
            if 'checked' in str(s):
                usluga_dopoln_2 = 'True'
            else:
                usluga_dopoln_2 = 'False'

            s = soup.find(id='paramU_AD_P')
            if 'checked' in str(s):
                usluga_dopoln_3 = 'True'
            else:
                usluga_dopoln_3 = 'False'

        ############################################

            s = soup.find(id='paramU_OK_R')
            if 'checked' in str(s):
                usluga_finish_1 = 'True'
            else:
                usluga_finish_1 = 'False'

            s = soup.find(id='paramU_OK_L')
            if 'checked' in str(s):
                usluga_finish_2 = 'True'
            else:
                usluga_finish_2 = 'False'

            s = soup.find(id='paramU_OK_G')
            if 'checked' in str(s):
                usluga_finish_3 = 'True'
            else:
                usluga_finish_3 = 'False'

        ############################################

            s = soup.find(id='paramU_M_C')
            if 'checked' in str(s):
                usluga_massage_1 = 'True'
            else:
                usluga_massage_1 = 'False'


            s = soup.find(id='paramU_M_P')
            if 'checked' in str(s):
                usluga_massage_2 = 'True'
            else:
                usluga_massage_2 = 'False'


            s = soup.find(id='paramU_M_R')
            if 'checked' in str(s):
                usluga_massage_3 = 'True'
            else:
                usluga_massage_3 = 'False'


            s = soup.find(id='paramU_M_TJ')
            if 'checked' in str(s):
                usluga_massage_4 = 'True'
            else:
                usluga_massage_4 = 'False'


            s = soup.find(id='paramU_M_U')
            if 'checked' in str(s):
                usluga_massage_5 = 'True'
            else:
                usluga_massage_5 = 'False'


            s = soup.find(id='paramU_M_T')
            if 'checked' in str(s):
                usluga_massage_6 = 'True'
            else:
                usluga_massage_6 = 'False'


            s = soup.find(id='paramU_M_E')
            if 'checked' in str(s):
                usluga_massage_7 = 'True'
            else:
                usluga_massage_7 = 'False'


            s = soup.find(id='paramU_M_S')
            if 'checked' in str(s):
                usluga_massage_8 = 'True'
            else:
                usluga_massage_8 = 'False'

        ############################################

            s = soup.find(id='paramU_ST_P')
            if 'checked' in str(s):
                usluga_strip_1 = 'True'
            else:
                usluga_strip_1 = 'False'

            s = soup.find(id='paramU_ST_NP')
            if 'checked' in str(s):
                usluga_strip_2 = 'True'
            else:
                usluga_strip_2 = 'False'

        ############################################

            s = soup.find(id='paramU_LS_O')
            if 'checked' in str(s):
                usluga_lesbi_1 = 'True'
            else:
                usluga_lesbi_1 = 'False'

            s = soup.find(id='paramU_LS_L')
            if 'checked' in str(s):
                usluga_lesbi_2 = 'True'
            else:
                usluga_lesbi_2 = 'False'

        ############################################

            s = soup.find(id='paramU_EXT_S')
            if 'checked' in str(s):
                usluga_extrim_1 = 'True'
            else:
                usluga_extrim_1 = 'False'

            s = soup.find(id='paramU_EXT_T')
            if 'checked' in str(s):
                usluga_extrim_2 = 'True'
            else:
                usluga_extrim_2 = 'False'

        ############################################

            s = soup.find(id='paramU_K_V')
            if 'checked' in str(s):
                usluga_copro_1 = 'True'
            else:
                usluga_copro_1 = 'False'


            s = soup.find(id='paramU_K_P')
            if 'checked' in str(s):
                usluga_copro_2 = 'True'
            else:
                usluga_copro_2 = 'False'


            #Получине фотографий
            arr_url = []  #Ссылки на все фото
            arr_photo = []
            photos_send = []

            number_p = 0
            # Первое фото, так как остальные в другом диве
            src_first = soup.find('div', class_='photo-block')
            src_first = src_first.find('div', class_='inner')

            for links in src_first.find_all('a'):
                arr_url.append(links.get('href'))

            try:
                src_second = soup.find('div', class_='gallery-box')

                for links in src_second.find_all('a'):
                    arr_url.append(links.get('href'))
            except:
                pass

            https_link_starts = 'https'

            for links in arr_url:
                if https_link_starts in links:
                    url = links
                    print(url)
                    img = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1'})
                    img = urllib.request.urlopen(img).read()
                    file_photo = "img%s.jpg" % (number_p)

                    out = open(file_photo, "wb")
                    out.write(img)
                    out.close
                    arr_photo.append(file_photo)
                    number_p += 1
                    print(url)
                else:
                    url = "https://msk8.prostitutki.today"+links
                    print(url)
                    img = urllib.request.urlopen(url).read()
                    file_photo = "img%s.jpg"%(number_p)

                    out = open(file_photo, "wb")
                    out.write(img)
                    out.close
                    arr_photo.append(file_photo)
                    number_p +=1
                    print(url)

            for i in arr_photo:
                photos_send.append(('photos', open(i, 'rb')))

            #что бы несколько фото  files = [('file', open('report.xls', 'rb')), ('file', open('report2.xls', 'rb'))]

            r = requests.post("http://185.183.96.77/test/", data={'anketa': '2', 'name': name, 'test_user': '1',
                                                                   'phone_number':phone_number, 'city': city, 'area':area,
                                                                   'metro':metro, 'girl_age': girl_age, 'girl_weight':girl_weight,
                                                                   'photo_approved':photo_approved, 'active_anketa':active_anketa, 'moderation':moderation,
                                                                    'girl_height':girl_height, 'girl_chest':girl_chest, 'exit_apartment':exit_apartment,
                                                                   'exit_sauna':exit_sauna, 'exit_hotel':exit_hotel, 'exit_office':exit_office,
                                                                    'her_apartment':her_apartment, 'hair_color':hair_color, 'intimate_haircut':intimate_haircut,
                                                                    'nationality':nationality, 'tariff_apartment_1':tariff_apartment_1, 'tariff_apartment_2':tariff_apartment_2,
                                                                    'tariff_apartment_night':tariff_apartment_night, 'tariff_exit_1':tariff_exit_1, 'tariff_exit_2':tariff_exit_2,
                                                                    'tariff_exit_night':tariff_exit_night, 'tariff_anal':tariff_anal, 'essay':essay, 'usluga_type_1':usluga_type_1,
                                                                    'usluga_type_2':usluga_type_2, 'usluga_type_3':usluga_type_3, 'usluga_type_4':usluga_type_4, 'usluga_laska_1':usluga_laska_1,
                                                                    'usluga_laska_2':usluga_laska_2, 'usluga_laska_3':usluga_laska_3, 'usluga_laska_4':usluga_laska_4,
                                                                   'usluga_laska_5':usluga_laska_5,'usluga_laska_6':usluga_laska_6, 'usluga_sado_mazo_1':usluga_sado_mazo_1,
                                                                   'usluga_sado_mazo_2':usluga_sado_mazo_2, 'usluga_sado_mazo_3':usluga_sado_mazo_3,'usluga_sado_mazo_4':usluga_sado_mazo_4,
                                                                   'usluga_sado_mazo_5':usluga_sado_mazo_5, 'usluga_sado_mazo_6':usluga_sado_mazo_6, 'usluga_sado_mazo_7':usluga_sado_mazo_7,
                                                                   'usluga_sado_mazo_8':usluga_sado_mazo_8, 'usluga_fisting_1':usluga_fisting_1, 'usluga_fisting_2':usluga_fisting_2,
                                                                   'usluga_zolotoy_1':usluga_zolotoy_1, 'usluga_zolotoy_2':usluga_zolotoy_2, 'usluga_dopoln_1':usluga_dopoln_1,
                                                                   'usluga_dopoln_2':usluga_dopoln_2, 'usluga_dopoln_3':usluga_dopoln_3, 'usluga_finish_1':usluga_finish_1,
                                                                   'usluga_finish_2':usluga_finish_2,'usluga_finish_3':usluga_finish_3,'usluga_massage_1':usluga_massage_1,
                                                                   'usluga_massage_2':usluga_massage_2, 'usluga_massage_3':usluga_massage_3, 'usluga_massage_4':usluga_massage_4,
                                                                   'usluga_massage_5':usluga_massage_5, 'usluga_massage_6':usluga_massage_6, 'usluga_massage_7':usluga_massage_7,
                                                                   'usluga_massage_8':usluga_massage_8,'usluga_strip_1':usluga_strip_1, 'usluga_strip_2':usluga_strip_2,
                                                                   'usluga_lesbi_1':usluga_lesbi_1, 'usluga_lesbi_2':usluga_lesbi_2, 'usluga_extrim_1':usluga_extrim_1,
                                                                   'usluga_extrim_2':usluga_extrim_2,'usluga_copro_1':usluga_copro_1, 'usluga_copro_2':usluga_copro_2


                                                                    }, files=photos_send)










            print(name, phone_number,city,metro,area, girl_age, girl_weight, girl_height, girl_chest , exit_apartment,
                  exit_hotel, exit_sauna,exit_office, her_apartment, hair_color, intimate_haircut, nationality,
                  tariff_apartment_1, tariff_apartment_2, tariff_apartment_night, tariff_anal, tariff_exit_1,
                  tariff_exit_2, tariff_exit_night, essay,
                  usluga_type_1,
                    usluga_type_2,
                    usluga_type_3,
                    usluga_type_4,
                    usluga_laska_1,
                    usluga_laska_2,
                    usluga_laska_3,
                    usluga_laska_4 ,
                    usluga_laska_5 ,
                    usluga_laska_6 ,
                    usluga_sado_mazo_1 ,
                    usluga_sado_mazo_2 ,
                    usluga_sado_mazo_3 ,
                    usluga_sado_mazo_4 ,
                    usluga_sado_mazo_5 ,
                    usluga_sado_mazo_6 ,
                    usluga_sado_mazo_7 ,
                    usluga_sado_mazo_8 ,
                    usluga_fisting_1 ,
                    usluga_fisting_2 ,
                    usluga_zolotoy_1 ,
                    usluga_zolotoy_2 ,
                    usluga_dopoln_1 ,
                    usluga_dopoln_2 ,
                    usluga_dopoln_3 ,
                    usluga_finish_1 ,
                    usluga_finish_2 ,
                    usluga_finish_3 ,
                    usluga_massage_1 ,
                    usluga_massage_2 ,
                    usluga_massage_3 ,
                    usluga_massage_4 ,
                    usluga_massage_5 ,
                    usluga_massage_6 ,
                    usluga_massage_7 ,
                    usluga_massage_8 ,
                    usluga_strip_1 ,
                    usluga_strip_2 ,
                    usluga_lesbi_1 ,
                    usluga_lesbi_2 ,
                    usluga_extrim_1 ,
                    usluga_extrim_2,
                    usluga_copro_1,
                    usluga_copro_2 ,
                    photos_send
                   )



        except:

            ex_silka = ex_silka + link + '\n'

    f_2.write("")
    f_2.write(ex_silka)
    f_2.close()

ppp()